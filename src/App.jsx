import { useState } from "react";
import "./App.css";
import { Toaster, toast } from "sonner";

function App() {
  const [base64Code, setBase64Code] = useState("");
  const [image, setImage] = useState("");

  const handleImageUpload = (e) => {
    let files = e.target.files;
    setImage(files[0]);
    console.log(files);
    files = files[0];
    if (files !== undefined) {
      if (files.size >= 5242880) {
        files = null;
        return;
      }
      let fileReader = new FileReader();
      fileReader.readAsDataURL(files);
      fileReader.onload = (event) => {
        const image64 = event.target.result;
        setBase64Code(image64);
        console.log(image64);
      };
    }
  };

  return (
    <>
      <Toaster position="bottom-center" theme="dark" />
      {/* <div className="container col-md-12 py-5">
        <div className="">
          <div className="py-3 chooser-container mb-3 my-3">
            <input
              className="chooser-input ms-3"
              type="file"
              onChange={handleImageUpload}
            />
          </div>
          <div className="row mb-3">
            <div className="col-md-6 mb-3">
              <div className="image-container rounded-4">
                <img
                  className="image-src p-3 rounded rounded-5"
                  src={
                    base64Code ||
                    "https://via.placeholder.com/800x400?text=Upload+Image"
                  }
                  alt="image64"
                />
              </div>
            </div>

            <div className="col-md-6 mb-3 ">
              <div className="rounded rounded-4  ">
                <div className="text-container p-4">
                  <small className="test1 text-white test1">
                    {base64Code}{" "}
                  </small>
                </div>
              </div>
            </div>

            <div className="mb-3">
              {base64Code && (
                <div>
                  {" "}
                  <div>
                    <span className="badge bg-black mb-3 me-2">
                      {image.size} Kb
                    </span>
                    <span className="badge bg-black mb-3">{image.name}</span>
                  </div>
                  <div>
                    <button
                      onClick={() => {
                        navigator.clipboard.writeText(base64Code),
                          toast("Text copy to clipboard!!");
                      }}
                      className="btn btn-copy"
                    >
                      <i className="bi bi-clipboard2-fill"></i> Copy
                    </button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div> */}

<div className="container col-md-12 py-5">
  <div className="">
    <div className="py-3 chooser-container mb-3 my-3">
      <input
        className="chooser-input ms-3"
        type="file"
        onChange={handleImageUpload}
      />
    </div>
    <div className="row mb-3">
      <div className="col-md-6 col-12 mb-3">
        <div className="image-container rounded-4">
          <img
            className="image-src p-3 rounded rounded-5"
            src={
              base64Code ||
              "https://via.placeholder.com/800x400?text=Upload+Image"
            }
            alt="image64"
          />
        </div>
      </div>

      <div className="col-md-6 col-12 mb-3">
        <div className="rounded rounded-4">
          <div className="text-container p-4">
            <small className="test1 text-white test1">{base64Code} </small>
          </div>
        </div>
      </div>

      <div className="mb-3">
        {base64Code && (
          <div>
            {" "}
            <div>
              <span className="badge bg-black mb-3 me-2">{image.size} Kb</span>
              <span className="badge bg-black mb-3">{image.name}</span>
            </div>
            <div>
              <button
                onClick={() => {
                  navigator.clipboard.writeText(base64Code),
                    toast("Text copy to clipboard!!");
                }}
                className="btn btn-copy"
              >
                <i className="bi bi-clipboard2-fill"></i> Copy
              </button>
            </div>
          </div>
        )}
      </div>
    </div>
  </div>
</div>

    </>
  );
}

export default App;
